#!/usr/bin/env python
# coding: utf-8

# # Mt. Everest Climbing Death
# ### By- Aarush Kumar
# ### Dated: November 26,2021

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# In[3]:


data = pd.read_csv("/home/aarush100616/Downloads/Projects/Mt Everest Deaths/Data/mount_everest_deaths.csv")
data


# In[4]:


data.info()


# In[5]:


data.isnull().sum()


# In[7]:


data.dtypes


# In[8]:


data.describe()


# In[9]:


data.describe().T


# ## Feature Engineering

# In[10]:


num_categories = [feature for feature in data.columns if data[feature].dtype != "O"]
data[num_categories].isnull().sum()


# In[12]:


df=pd.read_csv('/home/aarush100616/Downloads/Projects/Mt Everest Deaths/Data/mount_everest_deaths.csv',usecols=['Age','Cause of death','Location'])
df.head()
df.isnull().mean()


# ### Replacing the NaN values with the Random Sample Imputation

# #### Random sample imputation
# Random sample imputation consists of taking random observation from the dataset and we use this observation to replace the nan values

# In[15]:


def impute_nan(df,variable,median):
    df[variable+"_median"]=df[variable].fillna(median)
    df[variable+"_random"]=df[variable]
    ##It will have the random sample to fill the nan
    random_sample=df[variable].dropna().sample(df[variable].isnull().sum(),random_state=0)
    ##pandas need to have same index in order to merge the dataset
    random_sample.index=df[df[variable].isnull()].index   
    df.loc[df[variable].isnull(),variable+'_random']=random_sample
    impute_nan(df, "Age", median)


# In[16]:


def impute_nan(df,variable):
    ##It will have the random sample to fill the na
    random_sample=df[variable].dropna().sample(df[variable].isnull().sum(),random_state=0)
    ##pandas need to have same index in order to merge the dataset
    random_sample.index=df[df[variable].isnull()].index
    df.loc[df[variable].isnull(),'Age']=random_sample

impute_nan(data, "Age")


# In[17]:


cat_categories = [feature for feature in data.columns if data[feature].dtype == "O"]
data[cat_categories].isnull().sum()


# In[18]:


data[cat_categories] = data[cat_categories].fillna("unknown values")
data.head()


# In[19]:


plt.figure(figsize=(20,7))
sns.countplot(x="Nationality", data=data)
plt.title('Nationality Bar Graph')
plt.xticks(rotation=90)
plt.show()


# In[20]:


plt.figure(figsize=(20,7))
sns.countplot(x="Age", data=data)
plt.title('Person Age Bar Graph')
plt.xticks(rotation=90)
plt.show()


# In[21]:


plt.figure(figsize=(20,7))
sns.countplot(x="Cause of death", data=data)
plt.title('Cause of Death Bar Graph')
plt.xticks(rotation=90)
plt.show()


# In[22]:


plt.figure(figsize=(20,7))
sns.countplot(x="Expedition", data=data)
plt.title("Expedition Bar Graph")
plt.xticks(rotation=90)
plt.show()


# ### Result Of EDA
# * 1) According to the data in Nationality Nepal is the country that has taken part in Mt. Everest Climbing most number of times.
# * 2) According to the data Average age of the Explores of Mt. Everest is 27-35 years in this 28 and 33 are the age of the most Climbers.
# * 3) According to the data Avalanche, Fall, Exposure and Altitude Sickness are the most common way Climbers Die.
